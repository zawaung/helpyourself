<?php

include_once('conf.php');
include_once('mandrill/src/Mandrill.php');

class Mailer
{
    public static function send($content, $to, $name) {  
        $mandrill = new Mandrill(Conf::MANDRILL_KEY);
        $message = array(
            'html' => $content,
            'subject' => 'Your selected songs for 2014 Thingyan karaoke',
            'from_email' => 'webmaster@tisarana.org.au',
            'from_name' => 'Tisarana Monastery (Perth - Western Australia)',
            'to' => array (
                array (
                    'email' => $to,
                    'name' => $name,
                    'type' => 'to'
            )),
            'headers' => array('Reply-To' => 'noreply@tisarana.org.au'));

        $result = $mandrill->messages->send($message, false, 'Main Pool', '');
        
        return $result;
    }
}

function getMailContent($person, $songs)
{
    $content = file_get_contents('../mail_template.html');
    
    $content = str_replace('$FN', $person['first_name'], $content);
    $content = str_replace('$LN', $person['last_name'], $content);
    $content = str_replace('$EM', $person['email'], $content);
    $content = str_replace('$BD', $person['birth_date']->format('d/m/Y'), $content);

    $temp = '<table style="border: 1px solid black">' .
            '<thead><th colspan="3">SONGS SELECTION</th></thead>' .
            '<thead><th>Priority</th><th>Title</th><th>Artist</th></thead>' .
            '<tbody>';

    foreach ($songs as $row) {
        $temp .= '<tr>' .
                    '<td>' . $row['priority'] . '</td>' .
                    '<td>' . $row['title'] . '</td>' .
                    '<td>' . $row['artist'] . '</td>' .
                    '</tr>';
    }

    $temp .= '</tbody></table>';

    $content = str_replace('$SONGS', $temp, $content);

    return $content; 
}

?>
