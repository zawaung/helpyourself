<?php

class Conf
{
    const DB_DSN = 'mysql:dbname=song;host=localhost';
    const DB_USER = 'whatever';
    const DB_PWD = 'r1nd2zV0us';
    const MAX_SONG = 5;

    public static function getConnection() 
    {
        try {
            $pdo = new PDO(self::DB_DSN, self::DB_USER, self::DB_PWD);
            $pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false); 
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            return $pdo;
        }
        catch (PDOException $e) {
            var_dump($e);
        }
    }   
}

?>
