<?php
    if (isset($msg)) 
        echo "<div id='msg'>$msg</div>"
?>
<div class='page-head'>
    RETRIEVE RECORD
</div>
<div>
    <form method='post'>
        <input type='hidden' value='r' name='a' />
        <div id='person'>
            Direction : Fill in 'Email', then, 'Birth Date' as a security check and click 'Retrieve' button. Your previous record will be retrieve from the database and show up in a form so that you can update it.
            <table>
                <tr>
                    <td class='label'>Email</td>
                    <td class='input'>
                        <input type='text' id='person-email' name='person[email]' maxlength='200' size='100' />
                    </td>
                </tr>
                <tr>
                    <td class='label'>Birth Date</td>
                    <td class='input'>
                        <input type='text' id='person-dobd' name='person[dob][d]' maxlength='2' size='2' />&nbsp;/&nbsp; 
                        <input type='text' id='person-dobm' name='person[dob][m]' maxlength='2' size='2' />&nbsp;/&nbsp;
                        <input type='text' id='person-doby' name='person[dob][y]' maxlength='4' size='3' />&nbsp;(dd/mm/yyyy)
                    </td>
                </tr>
                <tr>
                    <td colspan='2'>
                        <br />
                        Prove that you are a human not a program bot by solving the math challenge below.<br /> 
                        <?php
                            echo generateChallenge();
                            echo "<input type='text' name='challenge-response' value='' size='2' maxlength='2' />";
                        ?>
                    </td>
                </tr>
            </table>
        </div>
        <div id='control'>
            <input type='submit' value='Retrieve' />
        </div>
    </form>
</div>
