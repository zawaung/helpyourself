<?php

include_once('test/conf.php');
include_once('person.php');

class PersonTest extends PHPUnit_Framework_TestCase
{
    private $dao = NULL;
    private $ids = array();
    private $pdo = NULL;

    public function __construct()
    {
        if (is_null($this->pdo))
            $this->pdo = Conf::getConnection();

        if (is_null($this->dao))
            $this->dao = new Person($this->pdo);
    }

    public function __destruct()
    {
        $this->dao = NULL;
        $this->pdo = NULL;
    }
    
    public function testInsertNullValueArray()
    {
        $this->setExpectedException('Exception');
        $this->dao->insert(NULL);
    }

    public function testInsertNullFirstName()
    {
        $this->setExpectedException('Exception');
        $this->dao->insert(array('first_name'=>NULL));
    }

    public function testInsertBlankFirstName()
    {
        $this->setExpectedException('Exception');
        $this->dao->insert(array('first_name'=>' '));
    }

    public function testInsertNullLastName()
    {
        $this->setExpectedException('Exception');
        $this->dao->insert(array('first_name'=>'first', 'last_name'=>NULL));
    }

    public function testInsertBlankLastName()
    {
        $this->setExpectedException('Exception');
        $this->dao->insert(array('first_name'=>'first', 'last_name'=>' '));
    }

    public function testInsertNullEmail()
    {
        $this->setExpectedException('Exception');
        $this->dao->insert(array('first_name'=>'first', 'last_name'=>'last', 'email'=>NULL));
    }

    public function testInsertBlankEmail()
    {
        $this->setExpectedException('Exception');
        $this->dao->insert(array('first_name'=>'first', 'last_name'=>'last', 'email'=>' '));
    }
    
    public function testInsertNullMobile()
    {
        $this->setExpectedException('Exception');
        $this->dao->insert(array('first_name'=>'first', 'last_name'=>'last', '
                                    email'=>'hello@world.com', 'mobile'=>NULL));
    }

    public function testInsertNullBirthDate()
    {
        $this->setExpectedException('Exception');
        $this->dao->insert(array('first_name'=>'first', 'last_name'=>'last', '
                                    email'=>'hello@world.com', 'mobile'=>'0450567890', 'birt_date'=>NULL));
    }

    public function testInsertBlankBirthDate()
    {
        $this->setExpectedException('Exception');
        $this->dao->insert(array('first_name'=>'first', 'last_name'=>'last', 
                                    'email'=>'hello@qorld.com', 'mobile'=>'0450567890', 'birth_date'=>' '));
    }
   

    public function testInsert()
    {
        $data = array('first_name'=>'first', 
                        'last_name'=>'last', 
                        'email'=>'hello@world.com',
                        'mobile'=>'0450567890',
                        'birth_date'=>'1975-03-11');

        $id = 0;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insert($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            echo $e->getMessage();
            $this->pdo->rollBack();
        }

        $this->assertTrue($id > 0);
        $this->ids[] = $id;
    }

    // test null value in id when calling select resulted in DAOException 
    public function testSelectNullID()
    {
        $this->setExpectedException('Exception');
        $this->dao->select(NULL);
    }

    // test non-numeric value in id when calling select resulted in DAOException 
    public function testSelectNonNumericID()
    {
        $this->setExpectedException('Exception');
        $this->dao->select('a');
    }

    // test calling select with valid id return an array with data
    public function testSelect()
    {
        $data = array('first_name'=>'first', 
                        'last_name'=>'last', 
                        'email'=>'hello@world.com',
                        'birth_date'=>'1975-03-11');

        $id = 0;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insert($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e);
            $this->pdo->rollBack();
        }

        $this->assertTrue($id > 0);

        $a = $this->dao->select($id);
        $this->ids[] = $id;

        $this->assertNotNull($a);
        $this->assertTrue(is_array($a));
        $this->assertEquals($data['first_name'], $a['first_name']);
        $this->assertEquals($data['last_name'], $a['last_name']);
        $this->assertEquals($data['email'], $a['email']);
        $this->assertEquals($data['birth_date'], $a['birth_date']);
      
    }
/*
    // test null value in id when calling delete resulted in DAOException 
    public function testDeleteNullID()
    {
        $this->setExpectedException('DAOException');
        $this->dao->delete(NULL);
    }

    // test non-numeric value in id when calling delete resulted in DAOException 
    public function testDeleteNonNumericID()
    {
        $this->setExpectedException('DAOException');
        $this->dao->delete('a');
    }

    // test calling delete with valid id work
    public function testDelete()
    {
        $data = array('creator'=>'1', 'title'=>'TEST', 'description'=> 'TESTING DELETE');
        $id = 0;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insert($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        try {
            $this->pdo->beginTransaction();
            $this->assertTrue($this->dao->delete($id));
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }
        
        $a = $this->dao->select($id);

        $this->assertTrue(count($a) <= 0);
    }

    // test null value in id when calling update resulted in DAOException 
    public function testUpdateNullID()
    {
        $this->setExpectedException('DAOException');
        $this->dao->update(NULL, array());
    }

    // test non-numeric value in id when calling update resulted in DAOException 
    public function testUpdateNonNumericID()
    {
        $this->setExpectedException('DAOException');
        $this->dao->update('a', array());
    }

    // test null values array when calling update results in DAOException
    public function testUpdateNullValueArray()
    {
        $this->setExpectedException('DAOException');
        $this->dao->update(1, NULL);
    }

    // test calling update work 
    public function testUpdate()
    {
        $data = array('creator'=>'1', 'title'=>'whatever', 'description'=> 'whatever');
        $id = 0;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insert($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        try {
            $this->pdo->beginTransaction();
            $this->dao->update($id, array('creator'=>2, 'title'=>'TEST', 'description'=>'TESTING UPDATE'));
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        $a = $this->dao->select($id);
        $this->assertTrue($a['creator'] == 2);
        $this->assertTrue($a['title'] == 'TEST');
        $this->assertTrue($a['description'] == 'TESTING UPDATE');
    }
    
    public function testCount()
    {
        $data = array('creator'=>'1', 'title'=>'TEST', 'description'=> 'TESTING COUNT');
        $id = 0;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insert($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }

        $cnt = $this->dao->count();
        $this->assertTrue($cnt > 0);
    }

    // test null values array when calling insert list resulted in DAOException 
    public function testInsertListNullList()
    {
        $this->setExpectedException('DAOException');
        $this->dao->insertList(NULL);
    }

    public function testInsertListNotArray()
    {
        $this->setExpectedException('DAOException');
        $this->dao->insertList('a');
    }
    
    // test null or '0' value in creator when calling insert list resulted in DAOException 
    public function testInsertListNullCreator()
    {
        $this->setExpectedException('DAOException');
        $this->dao->insertList(array(array()));
    }

    // test null or '0' value in title when calling insert list resulted in DAOException
    public function testInsertListNullTitle()
    {
        $this->setExpectedException('DAOException');
        $this->dao->insertList(array(array('creator'=>'1')));
    } 

    // test when calling insert list with valid values would function as expected 
    public function testInsertList()
    {
        $data = array();

        for ($i=1; $i<=20; $i++) {
            $data[] = array('creator'=>1, 'title'=>"TEST INSERT LIST $i", 'description'=>'TESTING INSERT LIST');
        }

        $id = NULL;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insertList($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }
        
        $this->assertTrue(is_array($id));
        $this->assertEquals(count($id), 20);
        $this->ids[] = array_merge($this->ids, $id);
    }

    public function testSelectListEmptyPage()
    {
        $this->setExpectedException('DAOException');
        $a = $this->dao->selectList('', 20);
    }

    public function testSelectListNonNumericPage()
    {
        $this->setExpectedException('DAOException');
        $a = $this->dao->selectList('a', 20);
    }

    public function testSelectListEmptyPageSize()
    {
        $this->setExpectedException('DAOException');
        $a = $this->dao->selectList(1, '');
    }

    public function testSelectListNonNumericPageSize()
    {
        $this->setExpectedException('DAOException');
        $a = $this->dao->selectList(1, 'a');
    }

    public function testSelectList()
    {
        $data = array();

        for ($i=1; $i<=20; $i++) {
            $data[] = array('creator'=>1, 'title'=>"TEST SELECT LIST $i", 'description'=>'TESTING SELECT LIST');
        }

        $id = NULL;

        try {
            $this->pdo->beginTransaction();
            $id = $this->dao->insertList($data);
            $this->pdo->commit();
        } 
        catch (Exception $e) {
            var_dump($e->getMessage());
            $this->pdo->rollBack();
        }
       
        $a = $this->dao->selectList(1, 20);
        $this->assertTrue(is_array($a));
        $this->assertTrue(count($a) >= 20);
        $this->assertTrue(is_array($a[0]));
    }
*/    
}

?>
