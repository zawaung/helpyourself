<?php

class Song 
{
    private $pdo = NULL;

    private $insertSql = 'INSERT INTO song (title, artist, priority, person) 
                            VALUES (:title, :artist, :priority, :person)';

    private $selectSql = 'SELECT * FROM song WHERE id = :id';

    private $searchSql = 'SELECT * FROM song WHERE person = :person ORDER BY priority';

    private $updateSql = 'UPDATE song SET title = :title, artist = :artist WHERE id = :id';

    private $deleteSql = 'DELETE FROM song WHERE id = :id';

    private $deleteByPersonSql = 'DELETE FROM song WHERE person = :person';

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function insert($values)
    {
        if (!isset($values))
            throw new Exception('The value array is empty!');

        if (!isset($values['title']) || strlen(trim($values['title'])) <= 0)
            throw new Exception('Empty title!');

        if (!isset($values['artist']) || strlen(trim($values['artist'])) <= 0)
            throw new Exception('Empty artist!');

        if (!isset($values['priority']) || strlen(trim($values['priority'])) <= 0)
            throw new Exception('Empty priority!');

        if (!is_numeric($values['priority']))
            throw new Exception('Priority should be numeric!');

        if (intval($values['priority']) < 1 || intval($values['priority']) > 5)
            throw new Exception('Priority should be between 1-5 inclusive!');

        if (!isset($values['person']) || strlen(trim($values['person'])) <= 0)
            throw new Exception('Empty person!');

        if (!is_numeric($values['person']))
            throw new Exception('Person should be numeric!');

        $id = 0;

        try {

            $stmt = $this->pdo->prepare($this->insertSql);

            $stmt->bindParam(':title', $values['title']);
            $stmt->bindParam(':artist', $values['artist']);
            $stmt->bindParam(':priority', $values['priority']);
            $stmt->bindParam(':person', $values['person']);

            try {
                $stmt->execute();
                $id = $this->pdo->lastInsertId(); 
            } 
            catch (PDOException $e) {
                $id = 0;
                throw new Exception('Error in the database!', 0, $e);
            }
        }   
        catch (PDOException $e) {
            throw new Exception('Error in the database!', 0, $e);
        }

        return $id;
    }

    public function select($id)
    {
        if (!isset($id)) 
            throw new Exception('Empty ID!');

        if (!is_numeric($id))
            throw new Exception('ID is not numeric');

        try {
            $stmt = $this->pdo->prepare($this->selectSql);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $a = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($a)
                return $a;
            else
                return array();
        }
        catch (PDOException $e) {
            throw new Exception('Error in the database!', 0, $e);
        }
    }

    public function search($person)
    {
        if (!isset($person)) 
            throw new Exception('Empty person id!');

        if (!is_numeric($person))
            throw new Exception('Person id is not numeric');

        try {
            $stmt = $this->pdo->prepare($this->searchSql);
            $stmt->bindParam(':person', $person);
            $stmt->execute();
            $a = $stmt->fetchAll(PDO::FETCH_ASSOC);
            if ($a)
                return $a;
            else
                return array();
        }
        catch (PDOException $e) {
            throw new Exception('Error in the database!', 0, $e);
        }
    }

    public function update($values)
    {
        if (!isset($values))
            throw new Exception('The value array is empty!');

        if (!isset($values['title']) || strlen(trim($values['title'])) <= 0)
            throw new Exception('Empty title!');

        if (!isset($values['artist']) || strlen(trim($values['artist'])) <= 0)
            throw new Exception('Empty artist!');

        $id = 0;

        try {
            $stmt = $this->pdo->prepare($this->updateSql);

            $stmt->bindParam(':title', $values['title']);
            $stmt->bindParam(':artist', $values['artist']);
            $stmt->bindParam(':id', $values['id']);
            $stmt->execute();
        }   
        catch (PDOException $e) {
            throw new Exception('Error in the database!', 0, $e);
        }
    }

    public function delete($id)
    {
        if (!isset($id)) 
            throw new Exception('Empty ID!');

        if (!is_numeric($id))
            throw new Exception('ID is not numeric');

        try {
            $stmt = $this->pdo->prepare($this->deleteSql);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
        }
        catch (PDOException $e) {
            throw new Exception('Error in the database!', 0, $e);
        }
    }

    public function deleteByPerson($person)
    {
        if (!isset($person)) 
            throw new Exception('Empty person id!');

        if (!is_numeric($person))
            throw new Exception('Person id is not numeric');

        try {
            $stmt = $this->pdo->prepare($this->deleteByPersonSql);
            $stmt->bindParam(':person', $person);
            $stmt->execute();
        }
        catch (PDOException $e) {
            throw new Exception('Error in the database!', 0, $e);
        }
    }

}

?>
