-- phpMyAdmin SQL Dump
-- version 4.1.4
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 06, 2014 at 09:08 PM
-- Server version: 5.6.12
-- PHP Version: 5.5.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `song`
--

-- --------------------------------------------------------

--
-- Table structure for table `history`
--

CREATE TABLE IF NOT EXISTS `history` (
	  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
	  `person` int(10) unsigned NOT NULL,
	  `date` datetime NOT NULL,
	  `action` tinyint(4) NOT NULL COMMENT '1-Create;2-update',
	  `ip` VARCHAR(50) NULL,
	  `host` VARCHAR(500) NULL,
	  PRIMARY KEY (`id`),
	  KEY `person` (`person`),
	  KEY `ip` (`ip`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

	-- --------------------------------------------------------

--
-- Table structure for table `person`
--

CREATE TABLE IF NOT EXISTS `person` (
	  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	  `first_name` varchar(50) NOT NULL,
	  `last_name` varchar(50) NOT NULL,
	  `email` varchar(200) NOT NULL,
	  `birth_date` date NOT NULL,
	  PRIMARY KEY (`id`),
	  UNIQUE KEY `email_2` (`email`),
	  KEY `email` (`email`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

	-- --------------------------------------------------------

--
-- Table structure for table `song`
--

CREATE TABLE IF NOT EXISTS `song` (
	  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
	  `title` varchar(200) NOT NULL,
	  `artist` varchar(100) NOT NULL,
	  `priority` tinyint(4) NOT NULL,
	  `person` int(11) NOT NULL,
	  PRIMARY KEY (`id`),
	  KEY `person` (`person`)
	) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
