<?php

include_once('../conf.php');
include_once('../data/person.php');
include_once('../data/song.php');
include_once('../data/history.php');
include_once('../mail.php');

$person = array();
$songs = array();
$pdo = Conf::getConnection();
$pdao = new Person($pdo);
$msg = NULL;

// FOR NEWLY MINT ADD 
if (!isset($_POST['person'])) {
    $person['id'] = 0;
    $person['first_name'] = '';
    $person['last_name'] = '';
    $person['birth_date'] = NULL;
    $person['email'] = '';
    $person['doby'] = '';
    $person['dobm'] = '';
    $person['dobd'] = '';

    for ($i=0;$i<5;$i++) {
        $songs[$i]['id'] = 0;
        $songs[$i]['title'] = '';
        $songs[$i]['artist'] = '';
        $songs[$i]['priority'] = 0;
    }
    $action = 'a';
    $view = '';
} 
else {
// ADD WHERE VALUES ARE FILLED UP ALREADY
    $valid = true;
    
    $person['id'] = 0;
    $person['first_name'] = filter_var($_POST['person']['first-name'], FILTER_SANITIZE_STRING);
    $person['last_name'] = filter_var($_POST['person']['last-name'], FILTER_SANITIZE_STRING);
    $person['email'] = filter_var($_POST['person']['email'], FILTER_SANITIZE_STRING);
    $person['doby'] = filter_var($_POST['person']['dob']['y'], FILTER_SANITIZE_STRING);
    $person['dobm'] = filter_var($_POST['person']['dob']['m'], FILTER_SANITIZE_STRING); 
    $person['dobd'] = filter_var($_POST['person']['dob']['d'], FILTER_SANITIZE_STRING);
    $person['birth_date'] = NULL; 

    $n = count($_POST['song']);
    $priority = 1;

    $songs = array();

    for($i=0;$i<$n;$i++) {
        $id = trim(filter_var($_POST['song'][$i]['id'], FILTER_SANITIZE_NUMBER_INT));
        $title = trim(filter_var($_POST['song'][$i]['title'], FILTER_SANITIZE_STRING));
        $artist = trim(filter_var($_POST['song'][$i]['artist'], FILTER_SANITIZE_STRING));

        if (strlen($title) > 0 && strlen($artist) > 0) {
            $songs[$i]['id'] = $id;
            $songs[$i]['title'] = $title;
            $songs[$i]['artist'] = $artist; 
            $songs[$i]['priority'] = $priority;
            $priority = $priority + 1;
        }
    }

    $answer = filter_var($_POST['challenge-response'], FILTER_VALIDATE_INT);
    $ok = ($_SESSION['answer'] == intval($answer));

    if (!$ok) {
        $msg = 'Your answer for math challenge to prove that you are a human is not correct. Please try again!';
    }
    else {
        if (strlen(trim($person['first_name'])) < 0) {
            $msg .= 'First name is balnk!<br />';
            $valid = false;
        }

        if (strlen(trim($person['last_name'])) < 0) {
            $msg .= 'Last name is balnk!<br />';
            $valid = false;
        }

        if (strlen(trim($person['email'])) < 0) {
            $msg .= 'Email is balnk!<br />';
            $valid = false;
        }

        if (!filter_var($person['email'], FILTER_VALIDATE_EMAIL)) {
            $msg .= 'Invalid email address format!<br />';
            $valid = false;
        }

        if (!is_numeric($person['doby'])) {
            $msg .= 'Non-numeric/blank value in birth date year! <br />';
            $valid = false;
        } 

        if (!is_numeric($person['dobm'])) {
            $msg .= 'Non-numeric/blank value in birth date month! <br />';
            $valid = false;
        } 

        if (!is_numeric($person['dobd'])) {
            $msg .= 'Non-numeric/blank value in birth date day! <br />';
            $valid = false;
        } 
        
        if (!checkdate(intval($person['dobm']), intval($person['dobd']), intval($person['doby']))) {
            $msg .= 'Invalid birth date!';
            $valid = false;
        }

        if ($valid) {       
            $person['birth_date'] = new DateTime('now', new DateTimeZone('Australia/Perth'));
            $person['birth_date']->setDate($person['doby'], $person['dobm'], $person['dobd']);

            $a = $pdao->search($person['email']);
            if (is_array($a) &&  count($a) > 0 && $a['id'] > 0) {
                $msg = 'Record exists for ' . $person['email'];
            }
            else {
                try {
                    $pdo->beginTransaction();
                    $pid = $pdao->insert($person);

                    $song = new Song($pdo);
                    $sid = array();
                    foreach ($songs as $row) {
                        $row['person'] = $pid;
                        $sid[] = $song->insert($row);
                    }
                    
                    $tempDate = new DateTime('now', new DateTimeZone('Australia/Perth'));

                    $log = new History($pdo);
                    $log->insert(array('person' => $pid, 
                                        'date' => $tempDate->format('Y-m-d H:i:s'), 
                                        'action' => History::CREATE));

                    $pdo->commit();

                    $msg = 'Successfully added your record to the database!';
                    $person = NULL;
                    $songs = NULL;

                    $person = $pdao->select($pid);
                    $songs = $song->search($pid); 

                    $person['dobd'] = $person['birth_date']->format('d');
                    $person['dobm'] = $person['birth_date']->format('m');
                    $person['doby'] = $person['birth_date']->format('y');    

                    Mailer::send(getMailContent($person, $songs), 
                                                    $person['email'], 
                                                    ucwords(trim($person['first_name'])) . ' ' . 
                                                    ucwords(trim($person['last_name'])));
                    $action = 'u';
                    $view = 'u';
                }
                catch (Exception $e) {
                    try {
                        $pdo->rollback();
                    }
                    catch (PDOException $pdoe) {
                    }
                    $msg = 'Error in database. Error message is (' . $e->getMessage() . ')';
                }

            }
        }
    }

    $n = count($songs);
    if ($n < 5) {
        for ($i=$n;$i<5;$i++) {
            $songs[$i]['id'] = 0;
            $songs[$i]['title'] = '';
            $songs[$i]['artist'] = '';
            $songs[$i]['priority'] = 0;
        }
    }
}


?>
