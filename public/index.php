<?php

$dueDate = DateTime::createFromFormat('Y-m-d H:i:s', '2014-3-31 23:59:59', new DateTimeZone('Australia/Perth'));
$now = new DateTime('now', new DateTimeZone('Australia/Perth'));

if ($now > $dueDate) {
    echo 'Due date (31 MAR 2014) has passed and Tisarana Thingyan karaoke songs selection is closed!';
    exit();
}

session_start();

function generateChallenge() {
    $number1 = mt_rand(1, 20);
    $number2 = mt_rand(1, 20);
    $number3 = mt_rand(1, 10);
    $question = 'The result of ';
    $answer = 0;

    if ( ($number3 % 2) > 0) { // MINUS
        if ($number2 > $number1) {
            $temp = $number1;
            $number1 = $number2;
            $number2 = $number1;
        } 
        $question .= "$number1 - $number2 is :";
        $answer = $number1 - $number2;    
    }
    else { // PLUS
        $question .= "$number1 + $number2 is :";
        $answer = $number1 + $number2;
    }

    $_SESSION['answer'] = $answer;
    return $question;
}


$action = '';
$view = '';

if (isset($_GET['a'])) 
    $action = filter_var($_GET['a'], FILTER_SANITIZE_STRING);

if (isset($_POST['a'])) 
    $action = filter_var($_POST['a'], FILTER_SANITIZE_STRING);

switch ($action) {
    case 's' : $view = 's'; break;
    case 'a' : include_once('../action/add.php'); break;
    case 'u' : include_once('../action/update.php'); break;
    case 'r' : include_once('../action/search.php'); break;
    case 't' : include_once('../action/topten.php'); break;
    default : $action = 'a'; include_once('../action/add.php'); break;
}

?>
<html>
<head>
    <title>Tisarana 2014 Thingyan Festival Celebration Karaoke Songs Request Form</title> 
    <script src="jquery.js"></script>
    <style>
        @import "main.css" screen,print;
    </style>
    <script>
        $(document).ready(function(){
                $(':input:enabled:visible:first').focus();
            }
        );
    </script>
</head>
<body>
<div id='content'>
<div id='title'>
<h1>Tisarana 2014 Thingyan Karaoke Songs Selection</h1>
</div>
<div id='command'>
<a href='?a='>Add New Selection</a>&nbsp;|&nbsp; 
<a href='?a=s'>Update Selection</a> &nbsp;|&nbsp; 
<a href='?a=t'>View Top Ten Selection</a>                
</div> 
<div id='main'>
<?php
    switch ($view) 
    {
        case 't' : include_once('../view/topten.php'); break;
        case 's': include_once('../view/search.php'); break;
        case 'u' : 
        default : include_once('../view/form.php'); break;
    }
?>
</div>
</div>
</body>
</html>
