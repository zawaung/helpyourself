<?php
    if (isset($msg)) 
        echo "<div id='msg'>$msg</div>"
?>
<div class='page-head'>
    <?php
        if ($action == 'a')
            echo 'ADD NEW SELECTION';
        else
            echo 'UPDATE SELECTION';
    ?>
</div>
<div>
    <form method='post'>
    <input type='hidden' name='a' value='<?php echo $action; ?>' />
        <div id='person'>
            <?php
                echo "<input type='hidden' id='person-id' name='person[id]' value='" . $person['id'] . "' />";
            ?>
            <table>
                <thead>
                    <th>Your Details</th>
                </thead>
                <tr>
                    <td class='label'>First Name :</td>
                    <td class='input'>
                    <?php
                        if ($action != 'a')
                            echo $person['first_name'];
                        else
                            echo "<input type='text' id='person-first-name' name='person[first-name]' value='" .  
                                    $person['first_name'] . "' maxlength='50' size='50' />";
                    ?>
                    </td>
                </tr>
                <tr>
                    <td class='label'>Last Name :</td>
                    <td class='input'>
                    <?php
                        if ($action !='a')
                            echo $person['last_name'];
                        else
                            echo "<input type='text' id='person-last-name' name='person[last-name]' value='" . 
                                $person['last_name'] . "' maxlength='50' size='50' />";
                    ?>
                    </td>
                </tr>
                <tr>
                    <td class='label'>Email :</td>
                    <td class='input'>
                    <?php
                        if ($action != 'a')
                            echo $person['email'];
                        else 
                            echo "<input type='text' id='person-email' name='person[email]' value='" . 
                                $person['email'] . "' maxlength='200' size='100'  />";
                    ?>
                    </td>
                </tr>
                <tr>
                    <td class='label'>Birth Date :</td>
                    <td class='input'>
                    <?php
                        if ($action != 'a') {
                            echo $person['birth_date']->format('d/m/Y');
                        } else {
                            echo "<input type='text' id='person-dobd' name='person[dob][d]' value='" . 
                                    $person['dobd'] . "' maxlength='2' size='2' />&nbsp;/&nbsp; " . 
                                    "<input type='text' id='person-dobm' name='person[dob][m]' value='" . 
                                    $person['dobm'] . "' maxlength='2' size='2' />&nbsp;/&nbsp;" . 
                                    "<input type='text' id='person-doby' name='person[dob][y]' value='" . 
                                    $person['doby'] . "' maxlength='4' size='3' />&nbsp;" .
                                    "(dd/mm/yyyy)";
                        }
                    ?>
                    </td>
                </tr>
            </table>
        </div>
        <div id='song'>
            <table>
                <thead>
                    <th colspan='3' style='text-align:left;'>Your Songs Selection</th>
                </thead>
                <thead>
                    <th>Priority</th>
                    <th>Title</th>
                    <th>Artist</th>
                </thead>
                <?php
                    for($i=0; $i<5;$i++) {
                        $id = "<input type='hidden' id='song-id-$i' name='song[$i][id]' value='" . $songs[$i]['id'] . "'/>";

                        echo "<tr>";
                        echo "<td>" . ($i + 1) . "$id</td>";

                        echo "<td><input id='song-title-$i' name='song[$i][title]' maxlength='200' size='100' value='" . 
                            $songs[$i]['title'] . "'/></td>";
                        echo "<td><input id='song-artist-$i' name='song[$i][artist]' maxlength='100' size='70' value='"  . 
                            $songs[$i]['artist'] . "'/></td>";

                        echo "</tr>";
                    }
                ?>
            </table>
        </div>
        <div>
            <br />
            Prove that you are a human not a program bot by solving the math challenge below.<br /> 
            <?php
                echo generateChallenge();
                echo "<input type='text' name='challenge-response' value='' size='2' maxlength='2' />";
            ?>
        </div>
        <div id='control'>
            <input type='submit' value='<?php if($action == 'a') echo 'Add'; else echo 'Update'; ?>' />
        </div>
    </form>
</div>
