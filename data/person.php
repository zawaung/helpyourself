<?php

class Person 
{
    private $pdo = NULL;

    private $insertSql = 'INSERT INTO person (first_name, last_name, email, birth_date) 
                    VALUES (:first_name, :last_name, :email, :birth_date)';

    private $selectSql = 'SELECT * FROM person WHERE id = :id';

    private $searchSql = 'SELECT * FROM person WHERE email = :email';

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function insert($values)
    {
        if (!isset($values))
            throw new Exception('The value array is empty!');

        if (!isset($values['first_name']) || strlen(trim($values['first_name'])) <= 0)
            throw new Exception('Empty first name!');

        if (!isset($values['last_name']) || strlen(trim($values['last_name'])) <= 0)
            throw new Exception('Empty last name!');

        if (!isset($values['email']) || strlen(trim($values['email'])) <= 0)
            throw new Exception('Empty email!');

        if (!isset($values['birth_date']) || is_null($values['birth_date']) )
            throw new Exception('Empty birth date!');

        if (!is_object($values['birth_date']) || get_class($values['birth_date']) != 'DateTime') 
            throw new Exception('Birth date is not of type DateTime!'); 

        $id = 0;

        try {

            $stmt = $this->pdo->prepare($this->insertSql);

            $stmt->bindParam(':first_name', $values['first_name']);
            $stmt->bindParam(':last_name', $values['last_name']);
            $stmt->bindParam(':email', $values['email']);
            $dob = $values['birth_date']->format('Y-m-d');
            $stmt->bindParam(':birth_date', $dob);

            try {
                $stmt->execute();
                $id = $this->pdo->lastInsertId(); 
            } 
            catch (PDOException $e) {
                $id = 0;
                throw new Exception('Error in the database!', 0, $e);
            }
        }   
        catch (PDOException $e) {
            throw new Exception('Error in the database!', 0, $e);
        }

        return $id;
    }

    public function select($id)
    {
        if (!isset($id)) 
            throw new Exception('Empty ID!');

        if (!is_numeric($id))
            throw new Exception('ID is not numeric');

        try {
            $stmt = $this->pdo->prepare($this->selectSql);
            $stmt->bindParam(':id', $id);
            $stmt->execute();
            $a = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($a) {
                $a['birth_date'] = DateTime::createFromFormat('Y-m-d', 
                                                                $a['birth_date'],
                                                                new DateTimeZone('Australia/Perth'));
                return $a;
            } 
            else
                return array();
        }
        catch (PDOException $e) {
            throw new Exception('Error in the database!', 0, $e);
        }
    }

    public function search($email)
    {
        if (!isset($email) || strlen(trim($email)) <= 0)
            throw new Exception('Empty email!');

        try {
            $stmt = $this->pdo->prepare($this->searchSql);
            $stmt->bindParam(':email', $email);
            $stmt->execute();
            $a = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($a) {
                $a['birth_date'] = DateTime::createFromFormat('Y-m-d', 
                                                                $a['birth_date'],
                                                                new DateTimeZone('Australia/Perth'));
                return $a;
            } 
            else
                return array();
        }
        catch (PDOException $e) {
            throw new Exception('Error in the database!', 0, $e);
        }

    }
}

?>
