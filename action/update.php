<?php

include_once('../conf.php');
include_once('../data/person.php');
include_once('../data/song.php');
include_once('../data/history.php');
include_once('../mail.php');

$person = array();

$pid = filter_var($_POST['person']['id'], FILTER_SANITIZE_NUMBER_INT);  

$n = count($_POST['song']);
$priority = 1;

$songs = array();

for($i=0;$i<$n;$i++) {
    $id  = trim(filter_var($_POST['song'][$i]['title'], FILTER_SANITIZE_NUMBER_INT));
    $title  = trim(filter_var($_POST['song'][$i]['title'], FILTER_SANITIZE_STRING));
    $artist = trim(filter_var($_POST['song'][$i]['artist'], FILTER_SANITIZE_STRING));

    if (strlen($title) > 0 && strlen($artist) > 0) {
        $songs[$i]['id'] = $id;
        $songs[$i]['title'] = $title;
        $songs[$i]['artist'] = $artist; 
        $songs[$i]['priority'] = $priority;
        $priority = $priority + 1;
    }
}

$pdo = Conf::getConnection();

$pdao = new Person($pdo);
$person = $pdao->select($pid);

$answer = filter_var($_POST['challenge-response'], FILTER_VALIDATE_INT);
$ok = ($_SESSION['answer'] == intval($answer));

if (!$ok) {
    $msg = 'Your answer for math challenge to prove that you are a human is not correct. Please try again!';
}
else {
    try {
        $pdo->beginTransaction();

        $song = new Song($pdo);
        $song->deleteByPerson($pid);
        $sid = array();
        foreach ($songs as $row) {
            $row['person'] = $pid;
            $sid[] = $song->insert($row);
        }

        $tempDate = new DateTime('now', new DateTimeZone('Australia/Perth'));

        $log = new History($pdo);
        $log->insert(array('person' => $pid, 
                            'date' => $tempDate->format('Y-m-d H:i:s'), 
                            'action' => History::UPDATE));

        $pdo->commit();

        $songs = NULL;

        $songs = $song->search($pid);
        $msg = 'Successfully updated your record!';
        Mailer::send(getMailContent($person, $songs), 
                                        $person['email'], 
                                        ucwords(trim($person['first_name'])) . ' ' . 
                                        ucwords(trim($person['last_name'])));
    }
    catch (Exception $e) {
        $pdo->rollback();
        $msg = 'Error in the database! Error message is (' . $e->getMessage() . ')';
    }

}

$n = count($songs);
if ($n < 5) {
    for ($i=$n;$i<5;$i++) {
        $songs[$i]['id'] = 0;
        $songs[$i]['title'] = '';
        $songs[$i]['artist'] = '';
        $songs[$i]['priority'] = 0;
    }
}

$action = 'u';
$view = 'u';

?>
