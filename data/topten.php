<?php

class TopTen 
{
    private $pdo = NULL;

    private $artistSql = 'SELECT artist, count(artist) AS cnt FROM song GROUP BY artist ORDER BY cnt DESC, artist LIMIT 10';
    private $songSql = 'SELECT title, artist, count(title) AS cnt FROM song GROUP BY title,artist ORDER BY cnt DESC, title';

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function getArtist()
    {
        return $this->pdo->query($this->artistSql);
    }

    public function getSong()
    {
        return $this->pdo->query($this->songSql);
    }
}

?>
