<?php

include_once('../conf.php');
include_once('../data/person.php');
include_once('../data/song.php');

$answer = filter_var($_POST['challenge-response'], FILTER_VALIDATE_INT);
$ok = ($_SESSION['answer'] == intval($answer));

if (!$ok) {
    $msg = 'Your answer for math challenge to prove that you are a human is not correct. Please try again!';
    $action = 's';
    $view = 's';
}
else {
    $email = filter_var($_POST['person']['email'], FILTER_SANITIZE_STRING);
    $doby = filter_var($_POST['person']['dob']['y'], FILTER_SANITIZE_NUMBER_INT);  
    $dobm = filter_var($_POST['person']['dob']['m'], FILTER_SANITIZE_NUMBER_INT); 
    $dobd = filter_var($_POST['person']['dob']['d'], FILTER_SANITIZE_NUMBER_INT);
    $dob = new DateTime('now', new DateTimeZone('Australia/Perth'));
    $dob->setDate($doby, $dobm, $dobd);

    $pdo = Conf::getConnection();
    $pdao = new Person($pdo);
    $person = $pdao->search($email);
    $songs = NULL;

    $action = '';
    $view = '';
    $msg = NULL;

    if (isset($person) && is_array($person) && count($person) > 0 &&  
            $dob->format('Y-m-d') == $person['birth_date']->format('Y-m-d')) { 

        $person['dobd'] = $person['birth_date']->format('d');
        $person['dobm'] = $person['birth_date']->format('m');
        $person['doby'] = $person['birth_date']->format('y');    

        $song = new Song($pdo);
        $songs = $song->search($person['id']);
        $n = count($songs);

        if ($n < 5) {
            for ($i=$n;$i<5;$i++) {
                $songs[$i]['id'] = 0;
                $songs[$i]['title'] = '';
                $songs[$i]['artist'] = '';
                $songs[$i]['priority'] = 0;
            }
        }

        $action = 'u';
        $view = 'u';
    } 
    else {
        $msg = 'Record not found for ' . $email;    
        $action = 's';
        $view = 's';
    }
}

?>
