<?php

class History 
{
    const CREATE = 1;
    const UPDATE = 2;

    private $pdo = NULL;

    private $insertSql = 'INSERT INTO history (person, date, action, ip, host) 
                            VALUES (:person, :date, :action, :ip, :host)';

    public function __construct($pdo)
    {
        $this->pdo = $pdo;
    }

    public function insert($values)
    {
        if (!isset($values))
            throw new Exception('The value array is empty!');

        $id = 0;

        $stmt = $this->pdo->prepare($this->insertSql);

        $stmt->bindParam(':person', $values['person']);
        $stmt->bindParam(':date', $values['date']);
        $stmt->bindParam(':action', $values['action']);
        $stmt->bindParam(':ip', $_SERVER['REMOTE_ADDR']);
        $stmt->bindParam(':host', gethostbyaddr($_SERVER['REMOTE_ADDR']));

        $stmt->execute();
        $id = $this->pdo->lastInsertId(); 

        return $id;
    }
}

?>
